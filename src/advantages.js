import React, {Component} from 'react';
import Rating from 'react-rating'
class Advantages extends Component {
    constructor(props) {
        super();
        console.log("init" + props);
        this.state = {
            advantage: 0
        };

        this.addAdvantage = this.addAdvantage.bind(this);
    };
    addAdvantage() {
        console.log("Advantages is: " + this.state.advantage);
        this.setState({
            advantage: this.state.advantage + 1
        });
    }

    render() {
        const children = [];
        console.log(this.props);
        for (var i = 0; i < this.state.advantage; i += 1) {
            children.push(<ChildComponent
                key={i} number={i}
                // advantages={this.props.advantages}
                // changeSelectedAdvantage={this.changeSelectedAdvantage}
                // selectdAdvantage={this.state.selectdAdvantage}
            />);
        }
        return (
            <div>
                <div className="row">
                    <div className="col-sm-12">
                        <h6 className="text-center adv-margin-padding mb-0">Advantages & Flaws
                            <button onClick={this.addAdvantage} type="button" className="btn btn-default p-0 mt-n1">
                                <i className="fas fa-plus-square"></i>
                            </button>
                        </h6>
                    </div>
                </div>
                {children}
            </div>
        );
    }
}

const ChildComponent = props =>
    <div className="row" key={props.number+"adr"}>
        <div className="col-sm-12" key={props.number+"adc"}>
            <div className="input-group">
                <select className="form-control input-group-text-background width-max-percentile solid-bottom-border" key={props.number}
                    // onChange={this.changeSelectedAdvantage}
                >
                    {/*<option>{props.advantages}</option>*/}
                </select>
                <div className="input-group-append">
                    <div className="input-group-text-background">
                        <Rating
                            // initialRating={this.props.selectdAdvantage.rating}
                            emptySymbol="far fa-circle"
                            fullSymbol="fas fa-circle"
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>;

export default Advantages;