import React, {Component} from 'react';
import Rating from 'react-rating'
class Skills extends Component {
    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }

    changeSpecialty ( event) {
        this.setState({specialty: event.target.value})
    }

    render() {
        console.log(this.props);
        return (
            <div className="input-group skill-col-min-width ">
                <div className="input-group-prepend">
                    <div className="input-group-text-background">{this.props.skill.skill}</div>
                </div>
                <input type="text" className="form-control dotted-bottom-border" value={this.props.skill.specialty} onChange={this.changeSpecialty.bind(this)}/>
                <div className="input-group-append">
                    <div className="input-group-text-background">
                        <Rating
                            initialRating={this.props.skill.rating}
                            onChange={this.ratingCompleted}
                            emptySymbol="far fa-circle"
                            fullSymbol="fas fa-circle"
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default Skills;