import React, {Component} from 'react';
import Rating from 'react-rating'
class Disciplines extends Component {
    constructor(props) {
        super();
        console.log("init" + props);
        this.state = {
            selectedDiscipline: "",
            rating: 0,
            knownDisciplines: props.knownDisciplines
        };

        this.ratingCompleted = this.ratingCompleted.bind(this);
        this.selectDiscipline = this.selectDiscipline.bind(this);
    };

    ratingCompleted(rating) {
        console.log("Rating is: " + rating);
        this.setState({
            rating: rating
        });
    }

    // updateStateOfSelectedDiscipline(rating, selectedDiscipline) {
    //     let known = this.state.knownDisciplines.splice();
    //     if(known.filter(kd => kd.name === selectedDiscipline).length > 0) {
    //         known.filter(kd => kd.name === selectedDiscipline).map(kd => kd.rating= rating);
    //     } else {
    //         known.push({ name: selectedDiscipline, rating: rating});
    //     }
    // }

    selectDiscipline(event) {
        console.log("selection is:" + event.target.value);
        this.setState({selectedDiscipline: event.target.value});
    }

    render() {
        const children = [];
        console.log(this.props);
        for (var i = 0; i < this.state.rating; i += 1) {
            children.push(<ChildComponent key={i} number={i} discipline={this.state.selectedDiscipline}/>);
        }
        return (
            <ParentComponent rating={this.state.rating} ratingCompleted={this.ratingCompleted} selectedDiscipline={this.selectDiscipline}
                             disciplines={this.props.disciplines} knownDisciplines={this.props.knownDisciplines}>
                {children}
            </ParentComponent>
        );
    }
}

const ParentComponent = props => (
    <div className="container">
        <div className="row">
            <div className="col-sm-12">
                <div className="input-group discipline-min-width">
                    <select className="form-control input-group-text-background dotted-bottom-border" onChange={props.selectedDiscipline}>
                        <option>&nbsp;</option>
                        {props.disciplines.map( disc =>
                            <option value={disc.name} key={disc.name}>{disc.name}</option>
                        )}
                    </select>
                    <div className="input-group-text-background rating-box" key="discRating">
                        <Rating
                            initialRating={props.rating}
                            onChange={props.ratingCompleted}
                            emptySymbol="far fa-circle"
                            fullSymbol="fas fa-circle"
                        />
                    </div>
                </div>
            </div>
        </div>
        {props.children}
    </div>
);

const ChildComponent = props =>
    <div className="row" key={props.number+props.discipline+"pr"}>
        <div className="col-sm-12" key={props.number+props.discipline+"pc"}>
            <select className="input-group-text-background width-max-percentile solid-bottom-border" key={props.number+props.discipline}>
                <option>{props.discipline}</option>
            </select>
        </div>
    </div>;

export default Disciplines;