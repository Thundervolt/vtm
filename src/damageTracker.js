import React, {Component} from 'react';
import Rating from 'react-rating'
class DamageTracker extends Component {
    ratingCompleted(event) {
        console.log("Rating is: " + event)
    }

    manageSymbols(isReadOnly) {
        let empty, full;
        if(this.props.tracker.style === "heart") {
            empty = "far fa-heart";
            full = "fas fa-heart";
            if(isReadOnly) {
                empty = "fas fa-heart-broken";
                full = empty;
            }
        } else if(this.props.tracker.style === "circle"){
            empty = "far fa-circle";
            full = "fas fa-circle";
            if(isReadOnly) {
                empty = "fas fa-times-circle";
                full = empty;
            }
        } else {
            empty = "far fa-square";
            // empty = <svgICON />;
            // full = "fas fa-th-large";
            full = "fas fa-window-close";
            if(isReadOnly) {
                empty = "fas fa-square";
                full = "fas fa-bolt fa-flip-horizontal custom-sm";
            }
        }
        return {emptySymbol: empty, fullSymbol: full};
    }

    render() {
        console.log(this.props);

        return (
            <div className={this.props.tracker.display === "inline" ? "input-group" : "row"}
                 key={this.props.tracker.description+"TrackerRow"}>

                <div className={this.props.tracker.display === "inline" ? "input-group-text-background" : "col-sm-12 text-center"}
                     key={this.props.tracker.description+"Label"}>{this.props.tracker.description}</div>
                <div className={this.props.tracker.display === "inline" ? "input-group-text-background" : "col-sm-12 text-center"}
                     key={this.props.tracker.description+"Rating"}>
                    {this.props.tracker.values.map( (value, index) =>
                        <Rating
                            initialRating={value.readonly? 2 : value.rating}
                            onChange={this.ratingCompleted}
                            stop={2}
                            step={2}
                            fractions={2}
                            emptySymbol={this.manageSymbols(value.readonly)["emptySymbol"] + " tracker-min-width"}
                            fullSymbol={this.manageSymbols(value.readonly)["fullSymbol"] + " tracker-min-width"}
                            readonly={value.readonly}
                            key={index+this.props.tracker.description}
                            id={index}
                        />
                    )}
                </div>
            </div>
        );
    }
}
const svgICON = () =>
    <svg role="img" viewBox="0 0 50 50">
        <path fill="currentColor"
              d="M144 32H32A32 32 0 0 0 0 64v384a32 32 0 0 0 32 32h112a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16H64V96h80a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zm272 0H304a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h80v320h-80a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h112a32 32 0 0 0 32-32V64a32 32 0 0 0-32-32z"
              className=""></path>
    </svg>;

export default DamageTracker;