import React from 'react';
import ReactDOM from 'react-dom';
import Rating from "react-rating";
import './scss/custom.scss';
import 'bootstrap';
import Advantages from './advantages';
import Attributes from './attributes';
import DamageTracker from "./damageTracker";
import Disciplines from "./disciplines";
import Skills from './skills';

class Sheet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            attributeHeaders: ["Physical", "Social", "Mental"],
            attributes: [{attribute:"Strength", rating: 0},
                {attribute:"Dexterity", rating: 0},
                {attribute:"Stamina", rating: 0},
                {attribute:"Charisma", rating: 0},
                {attribute:"Manipulation", rating: 0},
                {attribute:"Composure", rating: 0},
                {attribute:"Intelligence", rating: 0},
                {attribute:"Wits", rating: 0},
                {attribute:"Resolve", rating: 0}],
            attributeAssigned: Array.of(4,3,3,3,2,2,2,2,1),
            skills: [{skill:"Athletics", rating: 0, specialty: ""},
                {skill:"Brawl", rating: 0, specialty: ""},
                {skill:"Craft", rating: 0, specialty: ""},
                {skill:"Drive", rating: 0, specialty: ""},
                {skill:"Firearms", rating: 0, specialty: ""},
                {skill:"Larceny", rating: 0, specialty: ""},
                {skill:"Melee", rating: 0, specialty: ""},
                {skill:"Stealth", rating: 0, specialty: ""},
                {skill:"Survival", rating: 0, specialty: ""},
                {skill:"Animal", rating: 0, specialty: ""},
                {skill:"Etiquette", rating: 0, specialty: ""},
                {skill:"Insight", rating: 0, specialty: ""},
                {skill:"Intimidation", rating: 0, specialty: ""},
                {skill:"Leadership", rating: 0, specialty: ""},
                {skill:"Performance", rating: 0, specialty: ""},
                {skill:"Persuasion", rating: 0, specialty: ""},
                {skill:"Streetwise", rating: 0, specialty: ""},
                {skill:"Subterfuge", rating: 0, specialty: ""},
                {skill:"Academics", rating: 0, specialty: ""},
                {skill:"Awareness", rating: 0, specialty: ""},
                {skill:"Finance", rating: 0, specialty: ""},
                {skill:"Investigation", rating: 0, specialty: ""},
                {skill:"Medicine", rating: 0, specialty: ""},
                {skill:"Occult", rating: 0, specialty: ""},
                {skill:"Politics", rating: 0, specialty: ""},
                {skill:"Science", rating: 0, specialty: ""},
                {skill:"Technology", rating: 0, specialty: ""}],
            willpower: {description: "Willpower",
                display: "row",
                style: "square",
                values: [
                    {rating: 1, readonly: false},
                    {rating: 2, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: true},
                    {rating: 2, readonly: true},
                    {rating: 2, readonly: true}
                ]},
            health: {description: "Health",
                display: "row",
                style: "heart",
                values: [
                    {rating: 1, readonly: false},
                    {rating: 2, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: true},
                    {rating: 0, readonly: true},
                    {rating: 0, readonly: true},
                    {rating: 0, readonly: true},
                    {rating: 0, readonly: true}
                ]},
            hunger: {description: "Hunger",
                display: "inline",
                style: "square",
                values: [
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false}
                ]},
            humanity: {description: "Humanity",
                display: "inline",
                style: "square",
                values: [
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: false},
                    {rating: 0, readonly: true},
                    {rating: 0, readonly: true},
                    {rating: 0, readonly: true}

                ]},
            disciplines: [{name: "Potence"},{name: "Presence"}],
            knownDisciplines: [
                {name: "Potence", rating: 0},
                { name: "Presence", rating: 0}]
        };
    }

    render() {
        return (
            <div>
                <h5 className="card-title text-center">Attributes</h5>
                <hr className="my-1"/>
                <div className="row attrRow">
                    <div className="col-sm-1 meaningless-space">&nbsp;</div>
                    {Array.from(new Array(3),(val,index)=>index*3).map( id =>
                        <div className="col-sm-3 attribute-min-width" key={this.state.attributeHeaders[id/3]+"Col"}>
                            <p className="text-center">{this.state.attributeHeaders[id/3]}</p>
                            {this.state.attributes.slice(id,id+3).map(attribute =>
                                <Attributes attribute={attribute} key={attribute.attribute}/>
                            )}
                        </div>
                    )}
                    <div className="col-sm-1 meaningless-space">&nbsp;</div>
                </div>
                <div className="row">
                    <div className="col-sm-2">&nbsp;</div>
                    <div className="col-sm-3">
                        <DamageTracker tracker={this.state.health} key="health"/>
                    </div>
                    <div className="col-sm-2">&nbsp;</div>
                    <div className="col-sm-3">
                        <DamageTracker tracker={this.state.willpower} key="willpower"/>
                    </div>
                    <div className="col-sm-2">&nbsp;</div>
                </div>
                <h5 className="card-title text-center">Skills</h5>
                <hr className="my-1"/>
                <div className="row" key="skillsRow">
                    {Array.from(new Array(3),(val,index)=>index*9).map( id =>
                        <div className="col-sm-4 skill-col-min-width" key={id+"Col"} >
                            {this.state.skills.slice(id,id+9).map(skill =>
                                <Skills skill={skill} key={skill.skill}/>
                            )}
                        </div>
                    )}
                </div>
                <h5 className="card-title text-center">Disciplines</h5>
                <hr className="my-1"/>
                <div className="row">
                    <div className="col-sm-4">
                        <Disciplines disciplines={this.state.disciplines} knownDisciplines={this.state.knownDisciplines}/>
                    </div>
                    <div className="col-sm-4">
                        <Disciplines disciplines={this.state.disciplines} knownDisciplines={this.state.knownDisciplines}/>
                    </div>
                    <div className="col-sm-4">
                        <Disciplines disciplines={this.state.disciplines} knownDisciplines={this.state.knownDisciplines}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <Disciplines disciplines={this.state.disciplines} knownDisciplines={this.state.knownDisciplines}/>
                    </div>
                    <div className="col-sm-4">
                        <Disciplines disciplines={this.state.disciplines} knownDisciplines={this.state.knownDisciplines}/>
                    </div>
                    <div className="col-sm-4">
                        <Disciplines disciplines={this.state.disciplines} knownDisciplines={this.state.knownDisciplines}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <div className="input-group-text-background attribute-label">Resonance</div>
                            </div>
                            <input type="text" className="form-control solid-bottom-border" />
                        </div>
                    </div>
                    <div className="col-sm-4" style={{textAlign: "center"}}>
                        <div className="container" style={{display: "inline-block", width: "auto"}}>
                            <DamageTracker tracker={this.state.hunger} key="hunger"/>
                        </div>
                    </div>
                    <div className="col-sm-4" style={{textAlign: "right"}}>
                        <div className="container" style={{display: "inline-block", width: "auto"}}>
                            <DamageTracker tracker={this.state.humanity} key="humanity"/>
                        </div>
                    </div>
                </div>
                <div className="row my-4 mr-1 ml-1">
                    <div className="col-sm-4">
                        <h6 className="card-title text-center">Chronicle Tenets</h6>
                        <textarea className="width-max-percentile"/>
                    </div>
                    <div className="col-sm-4">
                        <h6 className="card-title text-center" >Touchstones & Convictions</h6>
                        <textarea className="width-max-percentile"/>
                    </div>
                    <div className="col-sm-4">
                        <h6 className="card-title text-center">Clan Bane</h6>
                        <textarea className="width-max-percentile"/>
                    </div>
                </div>
                <div className="row ml-1 height-max-percentile">
                    <div className="col-sm-6">
                        <div className="row">
                            <div className="col-sm-12">
                                <Advantages />
                            </div>
                        </div>
                        <div className="row border-top border-bottom border-secondary mt-2 mr-1">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <p className="generic-text-style">Notes</p>
                                <textarea className="width-max-percentile" />
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="row">
                                <div className="col-sm-3"></div>
                            <div className="col-sm-6">
                                <div className="input-group">
                                    <div className="input-group-text-background"><h6>Blood Potency</h6></div>
                                    <div className="form-control input-group-text-background" >
                                        <Rating
                                            stop={10}
                                            emptySymbol="far fa-circle"
                                            fullSymbol="fas fa-circle"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-3"></div>
                        </div>
                        <div className="row border-top border-bottom border-secondary mt-2 mr-1">
                            <div className="col-sm-6 border-left border-secondary">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text-background">Blood Surge</div>
                                    </div>
                                    <input className="form-control input-group-text-background width-max-percentile"/>
                                </div>
                                <div className="input-group ">
                                    <div className="input-group-text-background">Power Bonus</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                                <div className="input-group">
                                    <div className="input-group-text-background">Feeding Penalty</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                            </div>
                            <div className="col-sm-6 border-right border-left border-secondary">
                                <div className="input-group ">
                                    <div className="input-group-text-background">Mend Penalty</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                                <div className="input-group ">
                                    <div className="input-group-text-background">Rouse Re-Roll</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                                <div className="input-group">
                                    <div className="input-group-text-background">Bane Severity</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                            </div>
                        </div>
                        <div className="row mr-n1">
                            <div className="col-sm-12">
                                <div className="input-group ">
                                    <div className="input-group-text-background">Total Experience</div>
                                    <input className="form-control dotted-bottom-border"/>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3  mr-n1">
                            <div className="col-sm-12">
                                <div className="input-group ">
                                    <div className="input-group-text-background">Spent Experience</div>
                                    <input className="form-control dotted-bottom-border"/>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top border-secondary mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <div className="input-group">
                                    <div className="input-group-text-background">True Age</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top border-secondary mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <div className="input-group">
                                    <div className="input-group-text-background">Apparent Age</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top border-secondary mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <div className="input-group">
                                    <div className="input-group-text-background">Date of Birth</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top border-secondary  mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <div className="input-group">
                                    <div className="input-group-text-background">Date of Death</div>
                                    <input className="form-control input-group-text-background"/>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top border-secondary mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                    <p className="generic-text-style">Appearance</p>
                                    <textarea className="width-max-percentile"/>
                            </div>
                        </div>
                        <div className="row border-top border-secondary  mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <p className="generic-text-style">Distinguishing Features</p>
                                <textarea className="width-max-percentile"/>
                            </div>
                        </div>
                        <div className="row border-top border-bottom border-secondary mr-2">
                            <div className="col-sm-12 border-right border-left border-secondary">
                                <p className="generic-text-style">History</p>
                                <textarea className="width-max-percentile"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
ReactDOM.render(
    <Sheet />,
    document.getElementById('app')
);