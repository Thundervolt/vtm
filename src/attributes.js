import React, {Component} from 'react';
import Rating from 'react-rating'
class Attributes extends Component {
    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }

    render() {
        console.log(this.props);

        return (
            <div className="input-group ">
                    <div className="form-control input-group-text-background attribute-label">{this.props.attribute.attribute}</div>
                    <div className="input-group-text-background rating-box" >
                        <Rating
                            initialRating={this.props.attribute.rating}
                            onChange={this.ratingCompleted}
                            emptySymbol="far fa-circle"
                            fullSymbol="fas fa-circle"
                        />
                    </div>
            </div>
        );
    }
}

export default Attributes;